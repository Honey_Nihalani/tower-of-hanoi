﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Hanoi_Puzzel
{
    class Record_DB
    {
        public int noOfRecords;
        public string[] s_no, name, moves, time, seconds, date;
        private string data_to_write = "", data = "";
        private string[,] myData = new string[5, 6];
        private bool record_inserted, data_is_being_read = false;
        private StorageFile file;

        public Record_DB(StorageFile file)
        {
            this.file = file;
            s_no = new string[5];
            name = new string[5];
            moves = new string[5];
            time = new string[5];
            seconds = new string[5];
            date = new string[5];
            readFile();
        }

        public void setFile(StorageFile file)
        {
            this.file = file;
            readFile();
        }

        private async void readFile()
        {
            try
            {
                data = await FileIO.ReadTextAsync(file);
            }
            catch(NullReferenceException e)
            {

            }
        }

        public bool aligibleForRecord(int mov, int sec)
        {
            read_data();
            data_is_being_read = true;
            if (noOfRecords < 5)
                return true;
            else if ((mov < int.Parse(moves[noOfRecords - 1])) || ((mov == int.Parse(moves[noOfRecords - 1])) && (sec < int.Parse(seconds[noOfRecords - 1])))) 
                return true;
            return false;
        }

        public async void insert_record(string nm, string mov, string tm, int sec, string dt)
        {
            //read first of all current record
            if(!data_is_being_read)
                read_data();
            data_to_write = "";
            record_inserted = false;
            int i = 0, iterations;

            if (noOfRecords < 5)
                iterations = noOfRecords + 1;
            else
                iterations = noOfRecords;

            while (i < iterations)
            {
                if (!record_inserted && ((moves[i] == "") || (int.Parse(mov) < int.Parse(moves[i])) || ((int.Parse(mov) == int.Parse(moves[i])) && (sec < int.Parse(seconds[i])))))
                {
                    data_to_write += (i + 1) + ".$" + nm + "$" + mov + "$" + tm + "$" + sec + "$" + dt + "$$";
                    record_inserted = true;
                }
                else
                {
                    if (!record_inserted)
                        data_to_write += (i + 1) + ".$" + name[i] + "$" + moves[i] + "$" + time[i] + "$" + seconds[i] + "$" + date[i] + "$$";
                    else
                        data_to_write += (i + 1) + ".$" + name[i - 1] + "$" + moves[i - 1] + "$" + time[i - 1] + "$" + seconds[i - 1] + "$" + date[i - 1] + "$$";
                }
                i++;
            }

            await FileIO.WriteTextAsync(file, data_to_write);
            data_is_being_read = false;
            readFile();
        }

        public void read_data()
        {
            noOfRecords = 0;
            int i, j, k;
            for (i = 0; i < 5; i++)
                for (j = 0; j < 6; j++)
                    myData[i, j] = "";
            i = 0;
            j = 0;
            k = 0;
            char[] arr = data.ToCharArray();
            while (i < arr.Length)
            {
                if (arr[i] == '$')
                {
                    if (arr[i + 1] == '$')
                    {
                        noOfRecords++;
                        j++;
                        i++;
                        k = 0;
                    }
                    else
                        k++;
                    i++;
                }
                try
                {
                    myData[j, k] += arr[i];
                    i++;
                }
                catch (IndexOutOfRangeException e)
                {

                }
            }
            setStringValues();
        }

        private void setStringValues()
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    switch(j)
                    {
                        case 0: s_no[i] = myData[i, j]; break;
                        case 1: name[i] = myData[i, j]; break;
                        case 2: moves[i] = myData[i, j]; break;
                        case 3: time[i] = myData[i, j]; break;
                        case 4: seconds[i] = myData[i, j]; break;
                        case 5: date[i] = myData[i, j]; break;
                    }
                }
            }
        }

    }
}