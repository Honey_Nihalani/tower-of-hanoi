﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Hanoi_Puzzel
{

    public sealed partial class Hanoi : Page
    {
        Ellipse[] plates;
        Ellipse selectedEllipse;
        Rectangle[,] position;
        Rectangle[] myDes;
        Rectangle dialog_box;
        TextBlock header_of_dialog, other_data_about_dialog;
        TextBox players_name;
        Button button_1, button_2;
        TextBlock[] plateIndex;
        Thickness targetMargin;
        object[] data;
        double widthFector, heightFector;
        int number, selectedPlate, a = 0, b = 0, top = -1, sec = 0, min = 0, theme_no;
        bool[,] posStatus;
        bool displayingNumbers, displayingPillers, soundON, undoButtonClicked = false, won = false,
            valid_plate_by_pointer_enterd_event, game_over;
        string time = "", date = "", month = "", user_name = "", number_bit, piller_bit, sound_bit,
            warning_1 = "!!..There is plate above the plate which you are selecting..!!",
            warning_2 = "!!..A Larger one can't be placed over the smaller one..!!";
        Record_DB record;
        undo_redo_DB[] urDB;
        ComponentMaintainer cm;
        StorageFile file, user_preferences;
        SolidColorBrush selected_ellipse_color, unselected_ellipse_color, plate_fill_color;
        MediaElement move_forword, move_backword, wall_breaking;

        public Hanoi()
        {
            this.InitializeComponent();
            VariableInitialization();
            maintainAllComponents();
            open_honoi.Begin();
            open_honoi.Completed += open_honoi_Completed;
            Window.Current.SizeChanged += Current_SizeChanged;
        }

        void VariableInitialization()
        {
            cm = new ComponentMaintainer(60000);
            if (cm.transformComponents)
                cm.transformToOtherGrid(myGrid, other_grid, msg);
            widthFector = cm.widthFector;
            heightFector = cm.heightFector;
            move_forword = new MediaElement();
            move_backword = new MediaElement();
            wall_breaking = new MediaElement();
            move_forword.Source = new Uri(@"ms-appx:///sounds/place_plate.wav", UriKind.RelativeOrAbsolute);
            move_backword.Source = new Uri(@"ms-appx:///sounds/undo_move.wav", UriKind.RelativeOrAbsolute);
            wall_breaking.Source = new Uri(@"ms-appx:///sounds/wall_rolling.wav", UriKind.RelativeOrAbsolute);
            move_forword.AutoPlay = false;
            move_backword.AutoPlay = false;
            myGrid.Children.Add(move_forword);
            myGrid.Children.Add(move_backword);
            myGrid.Children.Add(wall_breaking);
        }

        void maintainAllComponents()
        {
            cm.maintainImage(background);
            cm.maintainImage(shatter_up);
            cm.maintainImage(shatter_down);

            cm.maintainButton(undo_button);
            cm.maintainButton(redo_button);

            cm.maintainRectangle(des1);
            cm.maintainRectangle(des2);
            cm.maintainRectangle(des3);
            cm.maintainRectangle(move_rect);
            cm.maintainRectangle(time_rect);

            cm.maintainTextblock(warning);
            cm.maintainTextblock(moves_title);
            cm.maintainTextblock(moves);
            cm.maintainTextblock(time_title);
            cm.maintainTextblock(minute);
            cm.maintainTextblock(coloun);
            cm.maintainTextblock(second);

            maintainOpenhanoiAnimation();

            Prefernces.Height *= heightFector;
            cm.maintainButton(options_new_game);
            cm.maintainButton(options_number);
            cm.maintainButton(options_piller);
            cm.maintainButton(options_sound);
            cm.maintainButton(options_reset_game);

            cm.maintainTextblock(msg);
        }

        void maintainOpenhanoiAnimation()
        {
            EasingDoubleKeyFrame key1, key2, key3, key4;
            key1 = new EasingDoubleKeyFrame();
            key2 = new EasingDoubleKeyFrame();
            key3 = new EasingDoubleKeyFrame();
            key4 = new EasingDoubleKeyFrame();

            key1.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.6));    key1.Value = 0;
            key2.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(3));      key2.Value = -Window.Current.Bounds.Height;
            key3.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.6));    key3.Value = 0;
            key4.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(3));      key4.Value = Window.Current.Bounds.Height;

            dauk_su_open.KeyFrames.Add(key1);
            dauk_su_open.KeyFrames.Add(key2);
            dauk_sd_open.KeyFrames.Add(key3);
            dauk_sd_open.KeyFrames.Add(key4);
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            switch (ApplicationView.Value)
            {
                case ApplicationViewState.FullScreenLandscape:
                    cm.transformToMainGrid(myGrid, other_grid);
                    break;
                case ApplicationViewState.FullScreenPortrait:
                    cm.transformToOtherGrid(myGrid, other_grid, msg);
                    break;
            }
        }

        void open_honoi_Completed(object sender, object e)
        {
            if (!game_over)
            {
                GeneratComponents();
                myTimer.Begin();
                myTimer.Completed += myTimer_Completed;
            }
            else
            {
                dialog_box_generation();
                if (won)
                    set_dialogbox(1);
                else
                    set_dialogbox(2);
            }
        }

        void myTimer_Completed(object sender, object e)
        {
            //posStatus[2,number-1] == true means player had complaetd the game..:)
            if (posStatus[2, number-1])
            {
                game_over = true;
                game_completed();
            } 
            else
            {
                set_new_time();
            }
        }

        void game_completed()
        {
            getDate();
            hideComponents();
            open_honoi.Stop();
            open_honoi.AutoReverse = true;
            cm.play_sound(wall_breaking);
            open_honoi.Begin();
            open_honoi.Seek(TimeSpan.FromSeconds(3));
            open_honoi.Completed += open_honoi_Completed;
            if (record.aligibleForRecord(int.Parse(moves.Text), (min * 60 + sec)))
            {
                won = true;
                string h = "00", m = min.ToString(), s = "00";
                if (min > 60)
                {
                    if ((int)(min / 60) < 10)
                        h = "0" + ((int)(min / 60)).ToString();
                    else
                        h = ((int)(min / 60)).ToString();
                    m = (min % 60).ToString();
                }
                if (int.Parse(m) < 10)
                    m = "0" + int.Parse(m);
                if (sec < 10)
                    s = "0" + sec;
                else
                    s = sec.ToString();
                time = h + " : " + m + " : " + s;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            data = e.Parameter as object[];

            number = (int)data[0];
            displayingNumbers = (bool)data[1];
            displayingPillers = (bool)data[2];
            soundON = (bool)data[3];
            file = (StorageFile)data[4];
            user_name = data[5].ToString();
            user_preferences = (StorageFile)data[6];
            theme_no = (int)data[7];

            set_preferences();
            set_theme_for_static_components();

            record = new Record_DB(file);
            urDB = new undo_redo_DB[number * 100];
            for (int i = 0; i < number * 100; i++)
                urDB[i] = new undo_redo_DB();
        }

        void GeneratComponents()
        {
            position = new Rectangle[3, number];
            plates = new Ellipse[number];
            posStatus = new bool[3, number];
            plateIndex = new TextBlock[number];
            myDes = new Rectangle[3];

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < number; j++)
                {
                    posStatus[i, j] = false;
                    position[i, j] = new Rectangle();
                    position[i, j].Height = 60;
                    position[i, j].Width = 10;
                    position[i, j].RadiusX = 5;
                    position[i, j].RadiusY = 15;
                    position[i, j].Margin = new Thickness(-900 + (900 * i), 500 - (150 * j), 0, 0);
                    cm.maintainRectangle(position[i, j]);
                    myGrid.Children.Add(position[i, j]);
                    if (!displayingPillers)
                        position[i, j].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
                myDes[i] = new Rectangle();
                myDes[i].Width = 420;
                myDes[i].Height = 210 + (number - 3) * 75;
                myDes[i].Margin = new Thickness(-900 + (900 * i), 350 - (number - 3) * 75, 0, 0);
                myDes[i].Fill = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
                myDes[i].Opacity = 0;
                myDes[i].PointerPressed += virtualDestinationClicked;
                cm.maintainRectangle(myDes[i]);
                myGrid.Children.Add(myDes[i]);
            }
            for (int i = 0; i < number; i++)
            {
                plates[i] = new Ellipse();
                plates[i].Height = 60;
                plates[i].Width = 450 - (i * 50);
                plates[i].StrokeThickness = 7;
                plates[i].Tag = "0" + i.ToString();
                plates[i].PointerPressed += pointer_pressed_on_plate;
                plates[i].PointerEntered += pointer_entered_in_plate;
                plates[i].Margin = new Thickness(-900, 500 - (150 * i), 0, 0);
                cm.maintaiEllipse(plates[i]);
                myGrid.Children.Add(plates[i]);
                position[0, i].Tag = i;
                posStatus[0, i] = true;
                plateIndex[i] = new TextBlock();
                plateIndex[i].Height = 50;
                plateIndex[i].Width = 50;
                plateIndex[i].FontSize = 45;
                plateIndex[i].Margin = new Thickness(-900, 500 - (150 * i), 0, 0);
                plateIndex[i].Text = (number - i).ToString();
                plateIndex[i].TextAlignment = TextAlignment.Center;
                plateIndex[i].PointerPressed += plateIndex_clicked;
                cm.maintainTextblock(plateIndex[i]);
                myGrid.Children.Add(plateIndex[i]);
                if (!displayingNumbers)
                    plateIndex[i].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            set_theme_for_dynamic_components();
            selectedPlate = number - 1;
            plates[number - 1].Stroke = selected_ellipse_color;
            selectedEllipse = plates[number - 1];
        }

        void dialog_box_generation()
        {
            dialog_box = new Rectangle();
            header_of_dialog = new TextBlock();
            other_data_about_dialog = new TextBlock();
            players_name = new TextBox();
            button_1 = new Button();
            button_2 = new Button();

            dialog_box.Width = 1376;
            dialog_box.Height = 250;
            dialog_box.Fill = new SolidColorBrush(Color.FromArgb(255, 42, 79, 99));
            dialog_box.StrokeThickness = 5;
            dialog_box.Stroke = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            dialog_box.Margin = new Thickness(-5, 0, 0, 0);
            cm.maintainRectangle(dialog_box);

            header_of_dialog.Height = 60;
            header_of_dialog.Width = 1000;
            header_of_dialog.FontSize = 35;
            header_of_dialog.Margin = new Thickness(0, -150, 0, 0);
            cm.maintainTextblock(header_of_dialog);

            other_data_about_dialog.Height = 50;
            other_data_about_dialog.Width = 1000;
            other_data_about_dialog.FontSize = 25;
            other_data_about_dialog.Margin = new Thickness(0, -20, 0, 0);
            cm.maintainTextblock(other_data_about_dialog);

            players_name.Height = 50;
            players_name.Width = 300;
            players_name.FontSize = 25;
            players_name.Text = user_name;
            players_name.Margin = new Thickness(-700, 100, 0, 0);
            cm.maintainTextbox(players_name);

            button_1.Height = 50;
            button_1.Width = 150;
            button_1.FontSize = 20;
            button_1.BorderThickness = new Thickness(0, 2, 0, 2);
            button_1.Click += button_Clicked;
            button_1.Margin = new Thickness(800, 150, 0, 0);
            cm.maintainButton(button_1);

            button_2.Height = 50;
            button_2.Width = 150;
            button_2.FontSize = 20;
            button_2.BorderThickness = new Thickness(0, 2, 0, 2);
            button_2.Click += button_Clicked;
            button_2.Margin = new Thickness(1000, 150, 0, 0);
            cm.maintainButton(button_2);

            myGrid.Children.Add(dialog_box);
            myGrid.Children.Add(header_of_dialog);
            myGrid.Children.Add(other_data_about_dialog);
            myGrid.Children.Add(players_name);
            myGrid.Children.Add(button_1);
            myGrid.Children.Add(button_2);
        }

        void set_dialogbox(int index)
        {
            /* index values detrmines...
             * 1.ask for player name
             * 2.insufficient record
             * 3.next choice 
             */
            switch (index)
            {
                case 1:
                    header_of_dialog.Text = "Heyy Congratulation.!! You had created the new record.!!";
                    other_data_about_dialog.Text = "You have completed this level in " + moves.Text + " MOVES and in " + time + " TIME";
                    button_1.Content = "Ok";
                    button_1.Tag = "6";
                    button_2.Content = "Cancel";
                    button_2.Tag = "1";
                    break;
                case 2:
                    header_of_dialog.Text = "Sorry.!! Your score is Insufficent.";
                    other_data_about_dialog.Text = "Lowest score in this level is " + record.moves[4] + " MOVES and in " + record.time[4] + " TIME";
                    players_name.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    button_1.Content = "Play Again";
                    button_1.Tag = "5";
                    button_2.Content = "New Game";
                    button_2.Tag = "1";
                    break;
                case 3:
                    if (number < 7)
                    {
                        header_of_dialog.Text = "Want to go for a Next level..??";
                        string level = "";
                        switch (number)
                        {
                            case 3:
                                level = "first";
                                break;
                            case 4:
                                level = "second";
                                break;
                            case 5:
                                level = "third";
                                break;
                            case 6:
                                level = "fourth";
                                break;
                        }
                        other_data_about_dialog.Text = "Currently you had played " + level + " level.";
                        players_name.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        button_1.Content = "New Game";
                        button_1.Tag = "1";
                        button_2.Content = "Next Level";
                        button_2.Tag = "7";
                    }
                    else
                    {
                        header_of_dialog.Text = "Congratulations.!! You have completed all levels..";
                        other_data_about_dialog.Text = "Want to play this level again..??";
                        players_name.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        button_1.Content = "Yes";
                        button_1.Tag = "5";
                        button_2.Content = "N0";
                        button_2.Tag = "1";
                    }
                    break;
            }
        }

        void set_theme_for_static_components()
        {
            switch (theme_no)
            {
                case 1:
                    background.Source = new BitmapImage(new Uri(@"ms-appx:///images/themes/Dark.jpg", UriKind.RelativeOrAbsolute));
                    des1.Fill = new SolidColorBrush(Color.FromArgb(255, 51, 56, 62));
                    des1.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
                    move_rect.Fill = new SolidColorBrush(Color.FromArgb(255, 127, 127, 127));
                    selected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
                    unselected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 0));
                    break;
                case 2:
                    background.Source = new BitmapImage(new Uri(@"ms-appx:///images/themes/Golden_rods.jpg", UriKind.RelativeOrAbsolute));
                    des1.Fill = new SolidColorBrush(Color.FromArgb(255, 199, 143, 16));
                    des1.Stroke = new SolidColorBrush(Color.FromArgb(255, 60, 0, 0));
                    move_rect.Fill = new SolidColorBrush(Color.FromArgb(255, 200, 150, 10));
                    selected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 201, 57));
                    unselected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 91, 43, 3));
                    break;
                case 3:
                    background.Source = new BitmapImage(new Uri(@"ms-appx:///images/themes/Purple_pattern.jpg", UriKind.RelativeOrAbsolute));
                    des1.Fill = new SolidColorBrush(Color.FromArgb(255, 123, 93, 67));
                    des1.Stroke = new SolidColorBrush(Color.FromArgb(255, 58, 47, 41));
                    move_rect.Fill = new SolidColorBrush(Color.FromArgb(255, 145, 119, 184));
                    selected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 210, 200, 240));
                    unselected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 100, 94, 106));
                    break;
                case 4:
                    background.Source = new BitmapImage(new Uri(@"ms-appx:///images/themes/Retro.jpg", UriKind.RelativeOrAbsolute));
                    des1.Fill = new SolidColorBrush(Color.FromArgb(255, 255, 127, 38));
                    des1.Stroke = new SolidColorBrush(Color.FromArgb(255, 137, 4, 0));
                    move_rect.Fill = new SolidColorBrush(Color.FromArgb(255, 88, 106, 20));
                    selected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 244, 171, 62));
                    unselected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 86, 13, 22));
                    break;
                case 5:
                    background.Source = new BitmapImage(new Uri(@"ms-appx:///images/themes/Wood_pattern.jpg", UriKind.RelativeOrAbsolute));
                    des1.Fill = new SolidColorBrush(Color.FromArgb(255, 160, 101, 59));
                    des1.Stroke = new SolidColorBrush(Color.FromArgb(255, 239, 202, 158));
                    move_rect.Fill = new SolidColorBrush(Color.FromArgb(255, 173, 109, 73));
                    selected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 218, 188, 154));
                    unselected_ellipse_color = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 49, 31, 27));
                    break;
            }
            des3.Fill = des2.Fill = des1.Fill;
            des3.Stroke = des2.Stroke = des1.Stroke;
            time_rect.Fill = undo_button.Background = redo_button.Background = options.Background = move_rect.Fill;
        }

        void set_theme_for_dynamic_components()
        {
            switch (theme_no)
            {
                case 1:
                    plate_fill_color = new SolidColorBrush(Color.FromArgb(255, 112, 146, 191));
                    break;
                case 2:
                    plate_fill_color = new SolidColorBrush(Color.FromArgb(255, 216, 113, 12));
                    break;
                case 3:
                    plate_fill_color = new SolidColorBrush(Color.FromArgb(255, 79, 56, 108));
                    break;
                case 4:
                    plate_fill_color = new SolidColorBrush(Color.FromArgb(255, 237, 27, 38));
                    break;
                case 5:
                    plate_fill_color = new SolidColorBrush(Color.FromArgb(255, 125, 78, 48));
                    break;
            }
            for (int i = 0; i < number; i++)
            {
                for (int j = 0; j < 3; j++)
                    position[j, i].Fill = selected_ellipse_color;
                plates[i].Fill = plate_fill_color;
                plates[i].Stroke = unselected_ellipse_color;
            }
        }

        void set_preferences()
        {
            if (!displayingNumbers)
            {
                options_number.Content = "Show Numbers";
                number_bit = "0";
            }
            else
                number_bit = "1";

            if (!displayingPillers)
            {
                options_piller.Content = "Show Pillers";
                piller_bit = "0";
            }
            else
                piller_bit = "1";

            if (!soundON)
            {
                move_backword.IsMuted = true;
                move_forword.IsMuted = true;
                wall_breaking.AutoPlay = false;
                wall_breaking.IsMuted = true;
                options_sound.Content = "Sound ON";
                sound_bit = "0";
            }
            else
                sound_bit = "1";
        }

        void pointer_entered_in_plate(object sender, PointerRoutedEventArgs e)
        {
            valid_plate_by_pointer_enterd_event = checkForValidPlate((Ellipse)sender);
            if (valid_plate_by_pointer_enterd_event)
                new_plate_selection((Ellipse)sender);
            else
                show_warning.Stop();
        }

        void pointer_pressed_on_plate(object sender, PointerRoutedEventArgs e)
        {
            if (valid_plate_by_pointer_enterd_event)
                new_plate_selection((Ellipse)sender);
            else
                show_warning_msg(warning_1);
        }

        void plateIndex_clicked(object sender, PointerRoutedEventArgs e)
        {
            pointer_pressed_on_plate(plates[number - int.Parse(((TextBlock)sender).Text)], e);
        }

        void new_plate_selection(Ellipse sender)
        {
            plates[selectedPlate].Stroke = unselected_ellipse_color;
            sender.Stroke = selected_ellipse_color;
            selectedEllipse = sender;
            int i = 0;
            while (!sender.Equals(plates[i]))
                i++;
            selectedPlate = i;
        }

        void virtualDestinationClicked(object sender, PointerRoutedEventArgs e)
        {
            if (sender.Equals(myDes[0]))
                destination_select(des1, e);
            else if (sender.Equals(myDes[1]))
                destination_select(des2, e);
            else
                destination_select(des3, e);
        }

        void destination_select(object sender, PointerRoutedEventArgs e)
        {
            if (validDestination((Rectangle)sender))
            {
                undoButtonClicked = false;

                //current position of selected plate is: (a,b)
                a = int.Parse(selectedEllipse.Tag.ToString().Substring(0, 1));
                b = int.Parse(selectedEllipse.Tag.ToString().Substring(1, 1));
                posStatus[a, b] = false;
                position[a, b].Tag = null;

                for (int i = 0; i < number; i++)
                {
                    if (!posStatus[int.Parse(((Rectangle)sender).Tag.ToString()), i])
                    {
                        cm.play_sound(move_forword);
                        targetMargin = position[int.Parse(((Rectangle)sender).Tag.ToString()), i].Margin;
                        selectedEllipse.Tag = ((Rectangle)sender).Tag.ToString() + i.ToString();
                        posStatus[int.Parse(((Rectangle)sender).Tag.ToString()), i] = true;
                        position[int.Parse(((Rectangle)sender).Tag.ToString()), i].Tag = selectedPlate;
                        break;
                    }
                }

                //make entry in DB and there is new position value now in seletedEllipse's Tag 
                updateMydb(a, b);
                selectedEllipse.Margin = targetMargin;
                plateIndex[selectedPlate].Margin = targetMargin;
                statusOfGame();
            }
            else
            {
                show_warning_msg(warning_2);
            }
        }

        bool checkForValidPlate(Ellipse clickedEllipse)
        {
            if (int.Parse(clickedEllipse.Tag.ToString().Substring(1, 1)) == (number - 1))
                return true;
            else if (!posStatus[int.Parse(clickedEllipse.Tag.ToString().Substring(0, 1)), int.Parse(clickedEllipse.Tag.ToString().Substring(1, 1)) + 1])
                return true;
            else
                return false;
        }

        bool validDestination(Rectangle destiRect)
        {
            if (!posStatus[int.Parse(destiRect.Tag.ToString()), 0])
                return true;
            for (int i = 1; i < number; i++)
            {
                if (!posStatus[int.Parse(destiRect.Tag.ToString()), i])
                {
                    if ((int)position[int.Parse(destiRect.Tag.ToString()), i - 1].Tag < selectedPlate)
                        return true;
                    break;
                }
            }
            return false;
        }

        async void preferences_changed(int bit)
        {
            switch (bit)
            {
                case 0:
                    break;
                case 1:
                    if (!displayingNumbers)
                        number_bit = "1";
                    else
                        number_bit = "0";
                    break;
                case 2:
                    if (!displayingPillers)
                        piller_bit = "1";
                    else
                        piller_bit = "0";
                    break;
                case 3:
                    if (!soundON)
                        sound_bit = "1";
                    else
                        sound_bit = "0";
                    break;
            }
            string temp = user_name + "$" + number_bit + "$" + piller_bit + "$" + sound_bit + "$" + theme_no + "$";
            await FileIO.WriteTextAsync(user_preferences, temp);
        }

        async void getNextFile()
        {
            if (number < 7)
            {
                switch (number + 1)
                {
                    case 4: data[4] = await ApplicationData.Current.LocalFolder.GetFileAsync("level_4.txt"); break;
                    case 5: data[4] = await ApplicationData.Current.LocalFolder.GetFileAsync("level_5.txt"); break;
                    case 6: data[4] = await ApplicationData.Current.LocalFolder.GetFileAsync("level_6.txt"); break;
                    case 7: data[4] = await ApplicationData.Current.LocalFolder.GetFileAsync("level_7.txt"); break;
                }
            }
        }

        void button_Clicked(object sender, RoutedEventArgs e)
        {
            switch(int.Parse(((Button)sender).Tag.ToString()))
            {
                case 1:
                    //New Game
                    this.Frame.Navigate(typeof(MainPage));
                    break;
                case 2:
                    //show or hide numbers
                    preferences_changed(1);
                    if (displayingNumbers)
                    {
                        for (int i = 0; i < number; i++)
                            plateIndex[i].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        displayingNumbers = false;
                        options_number.Content = "Show Numbers";
                    }
                    else
                    {
                        for (int i = 0; i < number; i++)
                            plateIndex[i].Visibility = Windows.UI.Xaml.Visibility.Visible;
                        displayingNumbers = true;
                        options_number.Content = "Hide Numbers";
                    }
                    break;
                case 3:
                    //pillers button
                    preferences_changed(2);
                    if (displayingPillers)
                    {
                        for (int i = 0; i < 3; i++)
                            for (int j = 0; j < number; j++)
                                position[i, j].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        displayingPillers = false;
                        options_piller.Content = "Show Pillers";
                    }
                    else
                    {
                        for (int i = 0; i < 3; i++)
                            for (int j = 0; j < number; j++)
                                position[i, j].Visibility = Windows.UI.Xaml.Visibility.Visible;
                        displayingPillers = true;
                        options_piller.Content = "Hide Pillers";
                    }
                    break;
                case 4:
                    //sound button
                    preferences_changed(3);
                    if (soundON)
                    {
                        move_forword.IsMuted = true;
                        move_backword.IsMuted = true;
                        wall_breaking.IsMuted = true;
                        options_sound.Content = "Sound ON";
                        soundON = false;
                    }
                    else
                    {
                        move_forword.IsMuted = false;
                        move_backword.IsMuted = false;
                        wall_breaking.IsMuted = false;
                        options_sound.Content = "Sound OFF";
                        soundON = true;
                    }
                    break;
                case 5:
                    //reset game
                    data[0] = number;
                    data[1] = displayingNumbers;
                    data[2] = displayingPillers;
                    data[3] = soundON;
                    data[4] = file;
                    data[5] = user_name;
                    data[6] = user_preferences;
                    this.Frame.Navigate(typeof(Hanoi), data);
                    break;
                case 6:
                    //insert record
                    if (players_name.Text != "")
                    {
                        if (players_name.Text != user_name)
                        {
                            user_name = players_name.Text;
                            verifyPlayerName();
                            preferences_changed(0);
                        }
                        record.insert_record(user_name, moves.Text, time, (min * 60) + sec, date);
                        set_dialogbox(3);
                        getNextFile();
                    }
                    break;
                case 7:
                    //next level
                    if (number < 7)
                    {
                        data[0] = number + 1;
                        data[1] = displayingNumbers;
                        data[2] = displayingPillers;
                        data[3] = soundON;
                        data[5] = user_name;
                        data[6] = user_preferences;
                        this.Frame.Navigate(typeof(Hanoi), data);
                    }
                    break;
                case 8:
                    try
                    {
                        //yes undoButton clicked...
                        undoButtonClicked = true;

                        //set previous positions of plate anf plate-index
                        urDB[top].plat.Margin = position[urDB[top].a1, urDB[top].b1].Margin;
                        urDB[top].PindexTB.Margin = position[urDB[top].a1, urDB[top].b1].Margin;
                        cm.play_sound(move_backword);

                        //current position (a2,b2) make flag to false there and position tag to null
                        posStatus[urDB[top].a2, urDB[top].b2] = false;
                        position[urDB[top].a2, urDB[top].b2].Tag = null;

                        //make previous position (a1,b1) flag to true and position tag
                        posStatus[urDB[top].a1, urDB[top].b1] = true;
                        position[urDB[top].a1, urDB[top].b1].Tag = urDB[top].platIndex;

                        //set the tag of plate as it where plate gone
                        urDB[top].plat.Tag = urDB[top].a1.ToString() + urDB[top].b1.ToString();

                        //deselect the currently selected plate
                        plates[selectedPlate].Stroke = unselected_ellipse_color;

                        //retrive the selected plate from db and make it as selected 
                        selectedPlate = urDB[top].platIndex;
                        plates[selectedPlate].Stroke = selected_ellipse_color;
                        selectedEllipse = urDB[top].plat;

                        //and ofcourse.. decrement the top pointer..:)
                        top--;

                        statusOfGame();
                    }
                    catch(IndexOutOfRangeException e1)
                    {

                    }
                    break;
                case 9:
                    try
                    {
                        undoButtonClicked = false;
                        top++;

                        //set previous positions of plate and plate-index
                        urDB[top].plat.Margin = position[urDB[top].a2, urDB[top].b2].Margin;
                        urDB[top].PindexTB.Margin = position[urDB[top].a2, urDB[top].b2].Margin;
                        cm.play_sound(move_forword);

                        //current position (a2,b2) make flag to false there and position tag to null
                        posStatus[urDB[top].a2, urDB[top].b2] = true;
                        position[urDB[top].a2, urDB[top].b2].Tag = urDB[top].platIndex;

                        //make previous position (a1,b1) flag to true and position tag
                        posStatus[urDB[top].a1, urDB[top].b1] = false;
                        position[urDB[top].a1, urDB[top].b1].Tag = null;

                        //set the tag of plate as it where plate gone
                        urDB[top].plat.Tag = urDB[top].a2.ToString() + urDB[top].b2.ToString();

                        //deselect the currently selected plate
                        plates[selectedPlate].Stroke = unselected_ellipse_color;

                        //retrive the selected plate from db and make it as selected 
                        selectedPlate = urDB[top].platIndex;
                        plates[selectedPlate].Stroke = selected_ellipse_color;
                        selectedEllipse = urDB[top].plat;

                        statusOfGame();
                    }
                    catch(NullReferenceException e1)
                    {
                        top--;
                    }
                    break;
             }
        }

        void updateMydb(int a, int b)
        {
            urDB[top + 1].plat = selectedEllipse;
            urDB[top + 1].PindexTB = plateIndex[selectedPlate];
            urDB[top + 1].a1 = a;
            urDB[top + 1].b1 = b;
            urDB[top + 1].a2 = int.Parse(selectedEllipse.Tag.ToString().Substring(0, 1));
            urDB[top + 1].b2 = int.Parse(selectedEllipse.Tag.ToString().Substring(1, 1));
            urDB[top + 1].platIndex = selectedPlate;
            top++;
        }

        void verifyPlayerName()
        {
            int i = 0;
            char[] arr = user_name.ToCharArray();
            user_name = "";
            while (i < arr.Length)
            {
                if (arr[i] == '$')
                    arr[i] = 'S';
                user_name += arr[i];
                i++;
            }
        }

        void set_new_time()
        {
            sec++;
            if (sec == 60)
            {
                sec = 0;
                min++;
                if (min < 10)
                    minute.Text = "0" + min.ToString();
                else
                    minute.Text = min.ToString();
            }
            if (sec < 10)
                second.Text = "0" + sec.ToString();
            else
                second.Text = sec.ToString();

            myTimer.Begin();
        }

        void getDate()
        {
            switch (DateTime.Today.Month)
            {
                case 1: month = "Jan"; break;
                case 2: month = "Feb"; break;
                case 3: month = "Mar"; break;
                case 4: month = "Apr"; break;
                case 5: month = "May"; break;
                case 6: month = "June"; break;
                case 7: month = "July"; break;
                case 8: month = "Aug"; break;
                case 9: month = "Sep"; break;
                case 10: month = "Oct"; break;
                case 11: month = "Nov"; break;
                case 12: month = "Dec"; break;
            }
            date = DateTime.Today.Day.ToString() + " " + month + " " + DateTime.Today.Year.ToString();
        }

        void hideComponents()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < number; j++)
                    position[i, j].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            for (int i = 0; i < number; i++)
            {
                plates[i].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                plateIndex[i].Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
        }

        void statusOfGame()
        {
            int count = int.Parse(moves.Text);
            if (!undoButtonClicked)
                count++;
            else
                count--;
            moves.Text = count.ToString();
        }

        void show_warning_msg(string text)
        {
            show_warning.Stop();
            warning.Text = text;
            show_warning.Begin();
        }

    }

    class undo_redo_DB
    {
        public Ellipse plat;
        public int platIndex, a1, b1, a2, b2;
        public TextBlock PindexTB;
    }

}