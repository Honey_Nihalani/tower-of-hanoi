﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Hanoi_Puzzel
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>    
    public sealed partial class MainPage : Page
    {
        string upData = "";
        double widthFector, heightFector;
        object[] data = new object[8];
        int theme_no;
        bool exit_without_click = true;
        string[] myData;
        ComponentMaintainer cm;
        Brush temp_color;
        SolidColorBrush white_color, selected_theme, pre_selected_theme;
        MediaElement crick_sound;
        StorageFile file_3, file_4, file_5, file_6, file_7, user_preferences;

        public MainPage()
        {
            this.InitializeComponent();
            myAnimation.AutoReverse = true;
            crick_sound = new MediaElement();
            crick_sound.Source = new Uri(@"ms-appx:///sounds/crick.wav", UriKind.RelativeOrAbsolute);
            crick_sound.AutoPlay = false;
            main_grid.Children.Add(crick_sound);
            myAnimation.Begin();
            myAnimation.Completed += myAnimation_Completed;
            getAllFiles();
            myData = new string[5];
            selected_theme = new SolidColorBrush(Color.FromArgb(255, 29, 204, 255));
            pre_selected_theme = new SolidColorBrush(Color.FromArgb(255, 68, 68, 68));
            white_color = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            cm = new ComponentMaintainer(50000);
            widthFector = cm.widthFector;
            heightFector = cm.heightFector;
            if (cm.transformComponents)
                cm.transformToOtherGrid(main_grid, other_grid, msg);
            maintainAllComponents();
            Window.Current.SizeChanged += Current_SizeChanged;
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            switch (ApplicationView.Value)
            {
                case ApplicationViewState.FullScreenLandscape:
                    cm.transformToMainGrid(main_grid, other_grid);
                    break;
                case ApplicationViewState.FullScreenPortrait:
                    cm.transformToOtherGrid(main_grid, other_grid, msg);
                    break;
            }
        }

        void maintainAllComponents()
        {
            cm.maintainImage(background);

            cm.maintainRectangle(r11);
            cm.maintainRectangle(r12);
            cm.maintainRectangle(r13);
            cm.maintainRectangle(r14);
            cm.maintainRectangle(r21);
            cm.maintainRectangle(r22);
            cm.maintainRectangle(r23);
            cm.maintainRectangle(r24);

            cm.maintainRectangle(theme_rod_1);
            cm.maintainRectangle(theme_rod_2);
            cm.maintainRectangle(theme_rod_3);
            cm.maintainRectangle(theme_rod_4);
            cm.maintainRectangle(theme_rod_5);

            cm.maintainRectangle(dark_theme);
            cm.maintainRectangle(golden_theme);
            cm.maintainRectangle(purple_theme);
            cm.maintainRectangle(retro_theme);
            cm.maintainRectangle(wood_theme);

            cm.maintainButton(startgame);
            cm.maintainButton(theme);
            cm.maintainButton(highscore);
            cm.maintainButton(aboutgame);

            cm.maintainImage(img_L3);
            cm.maintainImage(img_L4);
            cm.maintainImage(img_L5);
            cm.maintainImage(img_L6);
            cm.maintainImage(img_L7);

            cm.maintainTextblock(msg);

            maintainStartgameAnimation();
            maintainShowThemeAnimation();
        }

        void maintainStartgameAnimation()
        {
            EasingDoubleKeyFrame key1, key2, key3, key4, key5, key6, key7, key8, 
                                 key9, key10, key11, key12, key13, key14, key15, key16,
                                 key17, key18, key19, key20, key21, key22, key23, key24;
            key1 = new EasingDoubleKeyFrame();            key2 = new EasingDoubleKeyFrame();
            key3 = new EasingDoubleKeyFrame();            key4 = new EasingDoubleKeyFrame();
            key5 = new EasingDoubleKeyFrame();            key6 = new EasingDoubleKeyFrame();
            key7 = new EasingDoubleKeyFrame();            key8 = new EasingDoubleKeyFrame();
            key9 = new EasingDoubleKeyFrame();            key10 = new EasingDoubleKeyFrame();
            key11 = new EasingDoubleKeyFrame();            key12 = new EasingDoubleKeyFrame();
            key13 = new EasingDoubleKeyFrame();            key14 = new EasingDoubleKeyFrame();
            key15 = new EasingDoubleKeyFrame();            key16 = new EasingDoubleKeyFrame();
            key17 = new EasingDoubleKeyFrame();            key18 = new EasingDoubleKeyFrame();
            key19 = new EasingDoubleKeyFrame();            key20 = new EasingDoubleKeyFrame();
            key21 = new EasingDoubleKeyFrame();            key22 = new EasingDoubleKeyFrame();
            key23 = new EasingDoubleKeyFrame();            key24 = new EasingDoubleKeyFrame();

            KeyTime T_0, T_05;
            T_0 = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0));
            T_05 = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.5));

            double ww = Window.Current.Bounds.Width;

            key1.KeyTime = T_0; key1.Value = 0;
            key2.KeyTime = T_05; key2.Value = -ww;
            key3.KeyTime = T_0; key3.Value = 0;
            key4.KeyTime = T_05; key4.Value = ww;
            key5.KeyTime = T_0; key5.Value = 0;
            key6.KeyTime = T_05; key6.Value = ww;

            key19.KeyTime = T_0; key19.Value = 0;
            key20.KeyTime = T_05; key20.Value = ww;

            key7.KeyTime = T_0; key7.Value = 0;
            key8.KeyTime = T_05; key8.Value = -ww;
            key9.KeyTime = T_0; key9.Value = 0;
            key10.KeyTime = T_05; key10.Value = -ww;

            key11.KeyTime = T_0; key11.Value = 0;
            key12.KeyTime = T_05; key12.Value = ww;
            key13.KeyTime = T_0; key13.Value = 0;
            key14.KeyTime = T_05; key14.Value = ww;

            key15.KeyTime = T_0; key15.Value = 0;
            key16.KeyTime = T_05; key16.Value = ww;
            key17.KeyTime = T_0; key17.Value = 0;
            key18.KeyTime = T_05; key18.Value = ww;

            key21.KeyTime = T_0; key21.Value = 0;
            key22.KeyTime = T_05; key22.Value = ww;
            key23.KeyTime = T_0; key23.Value = 0;
            key24.KeyTime = T_05; key24.Value = ww;

            dauk_b1.KeyFrames.Add(key1);
            dauk_b1.KeyFrames.Add(key2);
            dauk_b2.KeyFrames.Add(key3);
            dauk_b2.KeyFrames.Add(key4);
            dauk_b3.KeyFrames.Add(key5);
            dauk_b3.KeyFrames.Add(key6);
            dauk_b4.KeyFrames.Add(key19);
            dauk_b4.KeyFrames.Add(key20);

            dauk_r11.KeyFrames.Add(key7);
            dauk_r11.KeyFrames.Add(key8);
            dauk_r21.KeyFrames.Add(key9);
            dauk_r21.KeyFrames.Add(key10);

            dauk_r12.KeyFrames.Add(key11);
            dauk_r12.KeyFrames.Add(key12);
            dauk_r22.KeyFrames.Add(key13);
            dauk_r22.KeyFrames.Add(key14);

            dauk_r13.KeyFrames.Add(key15);
            dauk_r13.KeyFrames.Add(key16);
            dauk_r23.KeyFrames.Add(key17);
            dauk_r23.KeyFrames.Add(key18);

            dauk_r14.KeyFrames.Add(key21);
            dauk_r14.KeyFrames.Add(key22);
            dauk_r24.KeyFrames.Add(key23);
            dauk_r24.KeyFrames.Add(key24);
        }

        void maintainShowThemeAnimation()
        {
            EasingDoubleKeyFrame key1, key2, key3, key4;
            key1 = new EasingDoubleKeyFrame();
            key2 = new EasingDoubleKeyFrame();
            key3 = new EasingDoubleKeyFrame();
            key4 = new EasingDoubleKeyFrame();
            key1.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0));   key1.Value = 0;
            key2.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.3)); key2.Value = 1128 * widthFector;
            key3.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0));   key3.Value = 0;
            key4.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.3)); key4.Value = 1128 * heightFector;

            dauk_theme_51.KeyFrames.Add(key1);
            dauk_theme_51.KeyFrames.Add(key2);
            dauk_theme_52.KeyFrames.Add(key3);
            dauk_theme_52.KeyFrames.Add(key4);

            EasingDoubleKeyFrame key5, key6, key7, key8; 
            key5 = new EasingDoubleKeyFrame();
            key6 = new EasingDoubleKeyFrame();
            key7 = new EasingDoubleKeyFrame();
            key8 = new EasingDoubleKeyFrame();
            key5.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.3));   key5.Value = 0;
            key6.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.6)); key6.Value = -1128 * widthFector;
            key7.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.3));   key7.Value = 0;
            key8.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.6)); key8.Value = 1128 * heightFector;

            dauk_theme_41.KeyFrames.Add(key5);
            dauk_theme_41.KeyFrames.Add(key6);
            dauk_theme_42.KeyFrames.Add(key7);
            dauk_theme_42.KeyFrames.Add(key8);

            EasingDoubleKeyFrame key9, key10, key11, key12;
            key9 = new EasingDoubleKeyFrame();
            key10 = new EasingDoubleKeyFrame();
            key11 = new EasingDoubleKeyFrame();
            key12 = new EasingDoubleKeyFrame();
            key9.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.6));   key9.Value = 0;
            key10.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.9));key10.Value = 1128 * widthFector;
            key11.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.6));  key11.Value = 0;
            key12.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.9));key12.Value = 1128 * heightFector;

            dauk_theme_31.KeyFrames.Add(key9);
            dauk_theme_31.KeyFrames.Add(key10);
            dauk_theme_32.KeyFrames.Add(key11);
            dauk_theme_32.KeyFrames.Add(key12);

            EasingDoubleKeyFrame key13, key14, key15, key16;
            key13 = new EasingDoubleKeyFrame();
            key14 = new EasingDoubleKeyFrame();
            key15 = new EasingDoubleKeyFrame();
            key16 = new EasingDoubleKeyFrame();
            key13.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.9));  key13.Value = 0;
            key14.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1.2));key14.Value = -1128 * widthFector;
            key15.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.9));  key15.Value = 0;
            key16.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1.2));key16.Value = 1128 * heightFector;

            dauk_theme_21.KeyFrames.Add(key13);
            dauk_theme_21.KeyFrames.Add(key14);
            dauk_theme_22.KeyFrames.Add(key15);
            dauk_theme_22.KeyFrames.Add(key16);

            EasingDoubleKeyFrame key17, key18, key19, key20;
            key17 = new EasingDoubleKeyFrame();
            key18 = new EasingDoubleKeyFrame();
            key19 = new EasingDoubleKeyFrame();
            key20 = new EasingDoubleKeyFrame();
            key17.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1.2));  key17.Value = 0;
            key18.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1.5));key18.Value = 1128 * widthFector;
            key19.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1.2));  key19.Value = 0;
            key20.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1.5));key20.Value = 1128 * heightFector;

            dauk_theme_11.KeyFrames.Add(key17);
            dauk_theme_11.KeyFrames.Add(key18);
            dauk_theme_12.KeyFrames.Add(key19);
            dauk_theme_12.KeyFrames.Add(key20);
        }

        async void getAllFiles()
        {
            try
            {
                file_3 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_3.txt");
                file_4 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_4.txt");
                file_5 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_5.txt");
                file_6 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_6.txt");
                file_7 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_7.txt");
                user_preferences = await ApplicationData.Current.LocalFolder.GetFileAsync("preferences.txt");
            }
            catch(Exception e)
            {
                creatFile(1);
                creatFile(3);
                creatFile(4);
                creatFile(5);
                creatFile(6);
                creatFile(7);
            }
        }

        async void creatFile(int file_no)
        {
            switch(file_no)
            {
                case 1: 
                    await ApplicationData.Current.LocalFolder.CreateFileAsync("preferences.txt");
                    user_preferences = await ApplicationData.Current.LocalFolder.GetFileAsync("preferences.txt");
                    await FileIO.WriteTextAsync(user_preferences, "Honey$1$1$1$3$");
                    break;
                case 3: 
                    await ApplicationData.Current.LocalFolder.CreateFileAsync("level_3.txt"); 
                    file_3 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_3.txt");
                    break;
                case 4: 
                    await ApplicationData.Current.LocalFolder.CreateFileAsync("level_4.txt"); 
                    file_4 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_4.txt");
                    break;
                case 5: 
                    await ApplicationData.Current.LocalFolder.CreateFileAsync("level_5.txt"); 
                    file_5 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_5.txt");
                    break;
                case 6: 
                    await ApplicationData.Current.LocalFolder.CreateFileAsync("level_6.txt"); 
                    file_6 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_6.txt");
                    break;
                case 7: 
                    await ApplicationData.Current.LocalFolder.CreateFileAsync("level_7.txt"); 
                    file_7 = await ApplicationData.Current.LocalFolder.GetFileAsync("level_7.txt");
                    break;
            }
        }

        async void readUserPreferencesFile()
        {
            upData = await FileIO.ReadTextAsync(user_preferences);
        }

        void read_data_of_UP()
        {
            int i = 0, j = 0;
            char[] arr = upData.ToCharArray();

            for (int k = 0; k < 5; k++)
                myData[k] = "";

            while (i < arr.Length)
            {
                if (arr[i] == '$')
                    j++;
                else
                    myData[j] += arr[i];
                i++;
            }
            data[5] = myData[0];

            if (myData[1] == "0")
                data[1] = false;
            else
                data[1] = true;

            if (myData[2] == "0")
                data[2] = false;
            else
                data[2] = true;

            if (myData[3] == "0")
                data[3] = false;
            else
                data[3] = true;

            theme_no = int.Parse(myData[4]);
        }

        void set_level_images()
        {
            String theme = "";
            switch(theme_no)
            {
                case 1:
                    theme = "Dark";
                    break;
                case 2:
                    theme = "Golden_rods";
                    break;
                case 3:
                    theme = "Purple_pattern";
                    break;
                case 4:
                    theme = "Retro";
                    break;
                case 5:
                    theme = "Wood_pattern";
                    break;
            }
            background.Source = new BitmapImage(new Uri(@"ms-appx:///images/themes/"+ theme +".jpg", UriKind.RelativeOrAbsolute));
            img_L3.Source = new BitmapImage(new Uri(@"ms-appx:///images/levels/" + theme + "/level_3.jpg", UriKind.RelativeOrAbsolute));
            img_L4.Source = new BitmapImage(new Uri(@"ms-appx:///images/levels/" + theme + "/level_4.jpg", UriKind.RelativeOrAbsolute));
            img_L5.Source = new BitmapImage(new Uri(@"ms-appx:///images/levels/" + theme + "/level_5.jpg", UriKind.RelativeOrAbsolute));
            img_L6.Source = new BitmapImage(new Uri(@"ms-appx:///images/levels/" + theme + "/level_6.jpg", UriKind.RelativeOrAbsolute));
            img_L7.Source = new BitmapImage(new Uri(@"ms-appx:///images/levels/" + theme + "/level_7.jpg", UriKind.RelativeOrAbsolute));
        }

        void button_Clicked(object sender, RoutedEventArgs e)
        {
            switch (int.Parse(((Button)sender).Tag.ToString()))
            {
                case 1:
                    cm.play_sound(crick_sound);
                    startGame.Begin();
                    startGame.Completed += startGame_Completed;
                    readUserPreferencesFile();
                    break;
                case 2:
                    cm.play_sound(crick_sound);
                    readUserPreferencesFile();
                    show_themes.AutoReverse = false;
                    show_themes.Begin();
                    show_themes.Completed += show_themes_Completed;
                    break;
                case 3:
                    StorageFile[] files = new StorageFile[5];
                    files[0] = file_3;
                    files[1] = file_4;
                    files[2] = file_5;
                    files[3] = file_6;
                    files[4] = file_7;
                    this.Frame.Navigate(typeof(HighScore), files);
                    break;
                case 4:
                    this.Frame.Navigate(typeof(AboutGame));
                    break;
            }
        }

        void startGame_Completed(object sender, object e)
        {
            move_levels.Begin();
            move_levels.Completed += move_levels_Completed;
            read_data_of_UP();
            set_level_images();
        }

        void move_levels_Completed(object sender, object e)
        {
            move_levels.Begin();
        }

        void show_themes_Completed(object sender, object e)
        {
            read_data_of_UP();
            set_stroke_color_of_theme(selected_theme);
        }

        void level_clicked(object sender, PointerRoutedEventArgs e)
        {
            switch(int.Parse(((Image)sender).Tag.ToString()))
            {
                case 3:
                    data[0] = 3;
                    data[4] = file_3;
                    break;
                case 4:
                    data[0] = 4;
                    data[4] = file_4;
                    break;
                case 5:
                    data[0] = 5;
                    data[4] = file_5;
                    break;
                case 6:
                    data[0] = 6;
                    data[4] = file_6;
                    break;
                case 7:
                    data[0] = 7;
                    data[4] = file_7;
                    break;
            }
            data[6] = user_preferences;
            data[7] = theme_no;
            this.Frame.Navigate(typeof(Hanoi), data);
        }

        void myAnimation_Completed(object sender, object e)
        {
            myAnimation.Begin();
        }

        void pointer_enterd_in_theme(object sender, PointerRoutedEventArgs e)
        {
            exit_without_click = true;
            temp_color = ((Rectangle)sender).Stroke;
            ((Rectangle)sender).Stroke = white_color;
            switch(int.Parse(((Rectangle)sender).Tag.ToString()))
            {
                case 1:
                    theme_rod_1.Fill = white_color;
                    break;
                case 2:
                    theme_rod_2.Fill = white_color;
                    break;
                case 3:
                    theme_rod_3.Fill = white_color;
                    break;
                case 4:
                    theme_rod_4.Fill = white_color;
                    break;
                case 5:
                    theme_rod_5.Fill = white_color;
                    break;
            }
        }

        void pointer_exited_from_theme(object sender, PointerRoutedEventArgs e)
        {
            if(exit_without_click)
            {
                ((Rectangle)sender).Stroke = temp_color;
                switch (int.Parse(((Rectangle)sender).Tag.ToString()))
                {
                    case 1:
                        theme_rod_1.Fill = temp_color;
                        break;
                    case 2:
                        theme_rod_2.Fill = temp_color;
                        break;
                    case 3:
                        theme_rod_3.Fill = temp_color;
                        break;
                    case 4:
                        theme_rod_4.Fill = temp_color;
                        break;
                    case 5:
                        theme_rod_5.Fill = temp_color;
                        break;
                }
            }
        }

        void pointer_pressed_on_theme(object sender, PointerRoutedEventArgs e)
        {
            exit_without_click = false;
            set_stroke_color_of_theme(pre_selected_theme);
            theme_no = int.Parse(((Rectangle)sender).Tag.ToString());
            set_stroke_color_of_theme(selected_theme);
            cm.play_sound(crick_sound);
            preferences_changed();
            show_themes.Stop();
            show_themes.AutoReverse = true;
            show_themes.Begin();
            show_themes.Seek(TimeSpan.FromSeconds(1.5));
            show_themes.Completed += show_themes_Completed_2;
        }

        void show_themes_Completed_2(object sender, object e)
        {
            theme_rod_1.Fill = dark_theme.Stroke =
            theme_rod_2.Fill = golden_theme.Stroke = 
            theme_rod_3.Fill = purple_theme.Stroke = 
            theme_rod_4.Fill = retro_theme.Stroke = 
            theme_rod_5.Fill = wood_theme.Stroke = pre_selected_theme;
        }

        async void preferences_changed()
        {
            string temp = myData[0] + "$" + myData[1] + "$" + myData[2] + "$" + myData[3] + "$" + theme_no + "$";
            await FileIO.WriteTextAsync(user_preferences, temp);
        }

        void set_stroke_color_of_theme(SolidColorBrush color)
        {
            switch (theme_no)
            {
                case 1:
                    theme_rod_1.Fill = color;
                    dark_theme.Stroke = color;
                    break;
                case 2:
                    theme_rod_2.Fill = color;
                    golden_theme.Stroke = color;
                    break;
                case 3:
                    theme_rod_3.Fill = color;
                    purple_theme.Stroke = color;
                    break;
                case 4:
                    theme_rod_4.Fill = color;
                    retro_theme.Stroke = color;
                    break;
                case 5:
                    theme_rod_5.Fill = color;
                    wood_theme.Stroke = color;
                    break;
            }
        }

    }
}