﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Hanoi_Puzzel
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HighScore : Page
    {
        int level_number = 3;
        double widthFector, heightFector;
        StorageFile[] files;
        Record_DB record;
        ComponentMaintainer cm;
        Rectangle[,] record_back;
        TextBlock[,] record_data;
        Rectangle[] record_rod, record_header_back, record_header_rod;
        TextBlock[] record_header_data;

        public HighScore()
        {
            this.InitializeComponent();
            cm = new ComponentMaintainer(45000);
            widthFector = cm.widthFector;
            heightFector = cm.heightFector;
            GenerateComponents();
            if (cm.transformComponents)
                cm.transformToOtherGrid(main_grid, other_grid, msg);
            maintainAllComponents();
            Window.Current.SizeChanged += Current_SizeChanged;
        }

        void GenerateComponents()
        {
            record_header_rod = new Rectangle[2];
            record_header_back = new Rectangle[5];
            record_header_data = new TextBlock[5];
            record_back = new Rectangle[5, 5];
            record_data = new TextBlock[5, 5];
            record_rod = new Rectangle[10];
            int jump = 0;

            for (int i = 0; i < 2; i++)
            {
                record_header_rod[i] = new Rectangle();
                record_header_rod[i].Height = 4;
                record_header_rod[i].Width = 1200;
                record_header_rod[i].Fill = new SolidColorBrush(Color.FromArgb(255, 192, 130, 86));
                record_header_rod[i].Margin = new Thickness(0, -290 + (20 * i), 0, 0);
                cm.maintainRectangle(record_header_rod[i]);
                main_grid.Children.Add(record_header_rod[i]);
            }

            for (int i = 0; i < 10; i++)
            {
                record_rod[i] = new Rectangle();
                record_rod[i].Height = 4;
                record_rod[i].Width = 1200;
                record_rod[i].Fill = new SolidColorBrush(Color.FromArgb(255, 220, 210, 204));
                record_rod[i].Margin = new Thickness(0, -40 + (20 * i) + (112 * jump), 0, 0);
                cm.maintainRectangle(record_rod[i]);
                main_grid.Children.Add(record_rod[i]);
                if(i % 2 != 0)
                    jump++;
            }

            for (int i = 0; i < 5; i++)
            {
                record_header_back[i] = new Rectangle();
                record_header_back[i].Height = 80;
                record_header_back[i].RadiusX = 20;
                record_header_back[i].RadiusY = 10;
                record_header_back[i].Fill = new SolidColorBrush(Color.FromArgb(255, 63, 36, 26));
                record_header_back[i].StrokeThickness = 5;
                record_header_back[i].Stroke = new SolidColorBrush(Color.FromArgb(255, 192, 130, 86));

                record_header_data[i] = new TextBlock();
                record_header_data[i].Height = 60;
                record_header_data[i].FontSize = 45;
                record_header_data[i].TextAlignment = TextAlignment.Center;

                switch (i)
                {
                    case 0:
                        record_header_data[i].Text = "#No.";
                        record_header_back[i].Width = 136;
                        record_header_back[i].Margin = new Thickness(-1175, -280, 0, 0);
                        break;
                    case 1:
                        record_header_data[i].Text = "Name";
                        record_header_back[i].Width = 346;
                        record_header_back[i].Margin = new Thickness(-640, -280, 0, 0);
                        break;
                    case 2:
                        record_header_data[i].Text = "Moves";
                        record_header_back[i].Width = 172;
                        record_header_back[i].Margin = new Thickness(-65, -280, 0, 0);
                        break;
                    case 3:
                        record_header_data[i].Text = "Time";
                        record_header_back[i].Width = 243;
                        record_header_back[i].Margin = new Thickness(375, -280, 0, 0);
                        break;
                    case 4:
                        record_header_data[i].Text = "Date";
                        record_header_back[i].Width = 331;
                        record_header_back[i].Margin = new Thickness(990, -280, 0, 0);
                        break;
                }
                record_header_data[i].Width = record_header_back[i].Width;
                record_header_data[i].Margin = record_header_back[i].Margin;
                cm.maintainTextblock(record_header_data[i]);
                cm.maintainRectangle(record_header_back[i]);
                main_grid.Children.Add(record_header_back[i]);
                main_grid.Children.Add(record_header_data[i]);

                for (int j = 0; j < 5; j++)
                {
                    record_back[i, j] = new Rectangle();
                    record_back[i, j].Height = 63;
                    record_back[i, j].Fill = new SolidColorBrush(Color.FromArgb(255, 87, 51, 32));
                    record_back[i, j].StrokeThickness = 4;
                    record_back[i, j].Stroke = new SolidColorBrush(Color.FromArgb(255, 220, 210, 204));
                    record_back[i, j].RadiusX = 20;
                    record_back[i, j].RadiusY = 10;

                    record_data[i, j] = new TextBlock();
                    record_data[i, j].Height = 50;
                    record_data[i, j].FontSize = 40;
                    record_data[i, j].Text = "--";
                    record_data[i, j].TextAlignment = TextAlignment.Center;

                    switch (i)
                    {
                        case 0:
                            record_back[i, j].Width = 110;
                            record_back[i, j].Margin = new Thickness(-1175, -25 + 150 * j, 0, 0);
                            break;
                        case 1:
                            record_back[i, j].Width = 330;
                            record_back[i, j].Margin = new Thickness(-640, -25 + 150 * j, 0, 0);
                            break;
                        case 2:
                            record_back[i, j].Width = 150;
                            record_back[i, j].Margin = new Thickness(-65, -25 + 150 * j, 0, 0);
                            break;
                        case 3:
                            record_back[i, j].Width = 225;
                            record_back[i, j].Margin = new Thickness(375, -25 + 150 * j, 0, 0);
                            break;
                        case 4:
                            record_back[i, j].Width = 310;
                            record_back[i, j].Margin = new Thickness(990, -25 + 150 * j, 0, 0);
                            break;
                    }
                    record_data[i, j].Width = record_back[i, j].Width;
                    record_data[i, j].Margin = record_back[i, j].Margin;
                    cm.maintainRectangle(record_back[i, j]);
                    cm.maintainTextblock(record_data[i, j]);
                    main_grid.Children.Add(record_back[i, j]);
                    main_grid.Children.Add(record_data[i, j]);
                }
            }
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            switch (ApplicationView.Value)
            {
                case ApplicationViewState.FullScreenLandscape:
                    cm.transformToMainGrid(main_grid, other_grid);
                    break;
                case ApplicationViewState.FullScreenPortrait:
                    cm.transformToOtherGrid(main_grid, other_grid, msg);
                    break;
            }
        }

        void maintainAllComponents()
        {
            cm.maintainImage(BackButton);

            cm.maintainTextblock(title_1);
            cm.maintainTextblock(title_2);
            cm.maintainTextblock(title_3);
            cm.maintainTextblock(level_no);

            cm.maintainImage(left_arrow);
            cm.maintainImage(right_arrow);

            cm.maintainTextblock(msg);

            cm.maintainRectangle(loading_bar);
            maintainLoadingAnimation();
        }

        void maintainLoadingAnimation()
        {
            EasingDoubleKeyFrame key1, key2;
            key1 = new EasingDoubleKeyFrame();
            key2 = new EasingDoubleKeyFrame();

            key1.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.05));   key1.Value = 100 * widthFector;
            key2.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(0.35));   key2.Value = 1285 * widthFector;

            dauk_load.KeyFrames.Add(key1);
            dauk_load.KeyFrames.Add(key2);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            files = e.Parameter as StorageFile[];
            record = new Record_DB(files[level_number - 3]);
            loading.Begin();
            hideData();
            loading.Completed += loading_Completed;
        }

        void arrow_clicked(object sender, PointerRoutedEventArgs e)
        {
            level_number = int.Parse(level_no.Text);

            if (sender.Equals(left_arrow))
            {
                if (level_number > 3)
                    level_number--;
                else
                    level_number = 7;
                left_arrow.Source = new BitmapImage(new Uri(@"ms-appx:///images/arrow/arrow_left_3.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                if (level_number < 7)
                    level_number++;
                else
                    level_number = 3;
                right_arrow.Source = new BitmapImage(new Uri(@"ms-appx:///images/arrow/arrow_right_3.png", UriKind.RelativeOrAbsolute));
            }

            record.setFile(files[level_number - 3]);
            level_no.Text = level_number.ToString();
            loading.Begin();
            loading.Completed += loading_Completed;
            hideData();
        }
  
        void loading_Completed(object sender, object e)
        {
            record.read_data();
            showData();
        }

        void hideData()
        {
            for (int i = 0; i < 5; i++)
                for(int j = 0; j<5; j++)
                    record_data[i, j].Text = "";
        }

        void showData()
        {
            for (int i = 0; i < 5; i++)
            { 
                for (int j = 0; j < 5; j++)
                {
                    switch(j)
                    {
                        case 0: record_data[j, i].Text = record.s_no[i]; break;
                        case 1: record_data[j, i].Text = record.name[i]; break;
                        case 2: record_data[j, i].Text = record.moves[i]; break;
                        case 3: record_data[j, i].Text = record.time[i]; break;
                        case 4: record_data[j, i].Text = record.date[i]; break;
                    }
                }
            }
        }

        void pointer_enterd_in_arrow(object sender, PointerRoutedEventArgs e)
        {
            if(sender.Equals(left_arrow))
                left_arrow.Source = new BitmapImage(new Uri(@"ms-appx:///images/arrow/arrow_left_2.png", UriKind.RelativeOrAbsolute));
            else
                right_arrow.Source = new BitmapImage(new Uri(@"ms-appx:///images/arrow/arrow_right_2.png", UriKind.RelativeOrAbsolute));
        }

        void pointer_exited_from_arrow(object sender, PointerRoutedEventArgs e)
        {
            if (sender.Equals(left_arrow))
                left_arrow.Source = new BitmapImage(new Uri(@"ms-appx:///images/arrow/arrow_left_1.png", UriKind.RelativeOrAbsolute));
            else
                right_arrow.Source = new BitmapImage(new Uri(@"ms-appx:///images/arrow/arrow_right_1.png", UriKind.RelativeOrAbsolute));
        }

        void pointer_released_in_arrow(object sender, PointerRoutedEventArgs e)
        {
            if (sender.Equals(left_arrow))
                left_arrow.Source = new BitmapImage(new Uri(@"ms-appx:///images/arrow/arrow_left_2.png", UriKind.RelativeOrAbsolute));
            else
                right_arrow.Source = new BitmapImage(new Uri(@"ms-appx:///images/arrow/arrow_right_2.png", UriKind.RelativeOrAbsolute));
        }

        void BackButton_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

    }
}
