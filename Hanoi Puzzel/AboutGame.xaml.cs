﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Hanoi_Puzzel
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AboutGame : Page
    {
        ComponentMaintainer cm;
        double widthFector, heightFector;
        int current_page = 1;
        Ellipse[] page_indicator;
        Brush currentPageIndicate, otherPageIndicate;

        public AboutGame()
        {
            this.InitializeComponent();

            currentPageIndicate = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            otherPageIndicate = new SolidColorBrush(Color.FromArgb(255, 110, 110, 110));
            cm = new ComponentMaintainer(75000);
            widthFector = cm.widthFector;
            heightFector = cm.heightFector;
            maintainComponent();
            generatePageIndicators();

            setPageIndicator(current_page);

            if (cm.transformComponents)
                cm.transformToOtherGrid(main_grid, other_grid, msg);
            Window.Current.SizeChanged += Current_SizeChanged;
        }

        void maintainComponent()
        {
            cm.maintainImage(BackButton);
            cm.maintainImage(screen_shot);
            cm.maintainImage(pre_page);
            cm.maintainImage(next_page);
            cm.maintainTextblock(Description);
        }

        void generatePageIndicators()
        {
            page_indicator = new Ellipse[11];
            for (int i = 0; i < 11; i++)
            {
                page_indicator[i] = new Ellipse();
                page_indicator[i] = new Ellipse();
                page_indicator[i].Width = 10;
                page_indicator[i].Height = 10;
                page_indicator[i].Fill = new SolidColorBrush(Color.FromArgb(255, 110, 110, 110));
                page_indicator[i].Margin = new Thickness(-400 + (i * 80), 720, 0, 0);
                cm.maintaiEllipse(page_indicator[i]);
                main_grid.Children.Add(page_indicator[i]);
            }
        }

        void setPageIndicator(int i)
        {
            page_indicator[i - 1].Fill = currentPageIndicate;
            page_indicator[i - 1].Width = 15 * widthFector;
            page_indicator[i - 1].Height = 15 * heightFector;
            if (i == 1)
            {
                page_indicator[i].Fill = otherPageIndicate;
                page_indicator[i].Width = 10 * widthFector;
                page_indicator[i].Height = 10 * heightFector;
            }
            else if (i == 11)
            {
                page_indicator[i - 2].Fill = otherPageIndicate;
                page_indicator[i - 2].Width = 10 * widthFector;
                page_indicator[i - 2].Height = 10 * heightFector;
            }
            else
            {
                page_indicator[i].Fill = otherPageIndicate;
                page_indicator[i - 2].Fill = otherPageIndicate;
                page_indicator[i].Width = 10 * widthFector;
                page_indicator[i].Height = 10 * heightFector;
                page_indicator[i - 2].Width = 10 * widthFector;
                page_indicator[i - 2].Height = 10 * heightFector;
            }
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            switch (ApplicationView.Value)
            {
                case ApplicationViewState.FullScreenLandscape:
                    cm.transformToMainGrid(main_grid, other_grid);
                    break;
                case ApplicationViewState.FullScreenPortrait:
                    cm.transformToOtherGrid(main_grid, other_grid, msg);
                    break;
            }
        }

        void changepage_pressed(object sender, PointerRoutedEventArgs e)
        {
            if (sender.Equals(next_page))
            {
                if (current_page < 11)
                    current_page++;
            }
            else
            {
                if (current_page > 1)
                    current_page--;
            }
            show_page();
        }

        void BackButton_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        void show_page()
        {
            setPageIndicator(current_page);
            screen_shot.Source = new BitmapImage(new Uri(@"ms-appx:///images/about_game/Screenshot_" + current_page + ".jpg", UriKind.RelativeOrAbsolute));
            switch (current_page)
            {
                case 1:
                    pre_page.Visibility = Visibility.Collapsed;
                    Description.Text = "This game has basically 5 levels, first level is with ‘3’ plates (easiest one) and fifth level is with ‘7’ plates (hardest one).";
                    break;
                case 2:
                    pre_page.Visibility = Visibility.Visible;
                    Description.Text = "After selecting level the game going to starts NOW...";
                    break;
                case 3:
                    Description.Text = "You have to transfer all the plates from source desk to destination desk, within minimum amount of time and less number of moves.";
                    break;
                case 4:
                    Description.Text = "At any time if you feels that the wrong move you have performed you can ‘UNDO’ and ‘REDO’ your action.";
                    break;
                case 5:
                    Description.Text = "Your goal state for first level (‘3’ plates) is such like this. As soon as you reach this state timer will automatically stops.";
                    break;
                case 6:
                    Description.Text = "You can’t place the plate with the larger value on the smaller one.";
                    break;
                case 7:
                    Description.Text = "And also you can’t select the plate on which even single plate is there."
                              +" \n" +"(Means at a time you can select top most plate only)";
                    break;
                case 8:
                    Description.Text = "There are several options regarding your preferences are there on option Appbar panel.";
                    break;
                case 9:
                    Description.Text = "You can store your record locally on your system.";
                    break;
                case 10:
                    next_page.Visibility = Visibility.Visible;
                    Description.Text = "On high scores page you can check top 5 high scores of each levels.";
                    break;
                case 11:
                    next_page.Visibility = Visibility.Collapsed;
                    Description.Text = "There are 5 attractive themes also available.";
                    break;
            }
        }

    }
}
