﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;

namespace Hanoi_Puzzel
{
    class ComponentMaintainer
    {
        public double widthFector, heightFector, fontDiff, windowWidth, windowHeight, stroke_fector;
        public bool transformComponents;
        Thickness margin;

        public ComponentMaintainer(double font)
        {
            windowWidth = Window.Current.Bounds.Width;
            windowHeight = Window.Current.Bounds.Height;

            if (ApplicationView.Value == ApplicationViewState.FullScreenPortrait)
            {
                if (windowHeight / windowWidth < 1.3)
                {
                    heightFector = windowHeight / 768;
                    widthFector = (windowHeight * 1.77864583333) / 1366;
                    fontDiff = (((windowHeight * windowHeight * 1.77) - (1366 * 768)) / font);
                }
                else
                {
                    heightFector = windowWidth / 768;
                    widthFector = windowHeight / 1366;
                    fontDiff = (((windowWidth * windowHeight) - (1366 * 768)) / font);
                }
                transformComponents = true;
            }
            else
            {
                widthFector = windowWidth / 1366;
                heightFector = windowHeight / 768;
                fontDiff = (((windowWidth * windowHeight) - (1366 * 768)) / font);
            }

            if(widthFector == heightFector)
            {
                stroke_fector = 1;
            }
            else if(widthFector < heightFector)
            {
                stroke_fector = widthFector;
            }
            else
            {
                stroke_fector = heightFector;
            }
        }

        public void transformToMainGrid(Grid main_grid, Grid other_grid)
        {
            main_grid.Visibility = Windows.UI.Xaml.Visibility.Visible;
            other_grid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        public void transformToOtherGrid(Grid main_grid, Grid other_grid, TextBlock msg)
        {
            main_grid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            other_grid.Visibility = Windows.UI.Xaml.Visibility.Visible;
            margin.Left = (Window.Current.Bounds.Width - msg.Width) / 2;
            margin.Top = (Window.Current.Bounds.Height - msg.Height) / 2;
            msg.Margin = margin;
        }

        public void maintaiEllipse(Ellipse ellipse)
        {
            ellipse.Width *= widthFector;
            ellipse.Height *= heightFector;
            ellipse.StrokeThickness *= stroke_fector;
            margin = ellipse.Margin;
            setMargin();
            ellipse.Margin = margin;
        }

        public void maintainRectangle(Rectangle rect)
        {
            rect.Width *= widthFector;
            rect.Height *= heightFector;
            rect.StrokeThickness *= stroke_fector;
            margin = rect.Margin;
            setMargin();
            rect.Margin = margin;
        }
        
        public void maintainTextblock(TextBlock textblock)
        {
            textblock.Width *= widthFector;
            textblock.Height *= heightFector;
            textblock.FontSize += fontDiff;
            margin = textblock.Margin;
            setMargin();
            textblock.Margin = margin;
        }

        public void maintainTextbox(TextBox textbox)
        {
            textbox.Width *= widthFector;
            textbox.Height *= heightFector;
            textbox.FontSize += fontDiff;
            margin = textbox.Margin;
            setMargin();
            textbox.Margin = margin;
        }

        public void maintainButton(Button button)
        {
            button.Width *= widthFector;
            button.Height *= heightFector;
            button.FontSize += (0.5 * fontDiff);
            margin = button.Margin;
            setMargin();
            button.Margin = margin;
            margin = button.BorderThickness;
            setMargin();
            button.BorderThickness = margin;
        }

        public void maintainImage(Image image)
        {
            image.Width *= widthFector;
            image.Height *= heightFector;
            margin = image.Margin;
            setMargin();
            image.Margin = margin;
        }

        void setMargin()
        {
            margin.Top *= heightFector;
            margin.Left *= widthFector;
            margin.Bottom *= heightFector;
            margin.Right *= widthFector;
        }

        public void play_sound(MediaElement sound)
        {
            sound.Position = TimeSpan.FromSeconds(0);
            sound.Play();
        }

    }
}
